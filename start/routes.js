'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.get('/create_orgchart', 'OrgController.create')
Route.get('/get_objhierarchy/:key', 'OrgController.getObjHierarchy')
Route.get('/get_orgchart/:key', 'OrgController.getOrgChartTest')
Route.post('/get_adfs', 'ADFSController.getADFSTest')



