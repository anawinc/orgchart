'use strict'
const fs = use("fs");
const Helpers = use("Helpers");

//for XML parsing
var xml2js = use('xml2js');
var https = use('https');
//to process WS-Trust requests
var trustClient = use('wstrust-client');

class ADFSController {
    

    async getADFSTest ({ request,params, response }) {
        try {
          console.log('start')
          
          let userName = request.body.user;
          let userPassword = request.body.password;
          let dataReturn = {}
          let dataRaw = {}
          //call endpoint, and pass in values
          let aa = new Promise((resolve, reject) => { trustClient.requestSecurityToken({
              scope: 'https://testadfs.gsb.or.th/adfs/services/npla',
              username: userName,
              password: userPassword,
              endpoint: 'https://testadfs.gsb.or.th/adfs/services/trust/13/usernamemixed'
          }, (rstr) => {
       
              // Access the token
              let rawToken = rstr.token;
       
              //convert to json
              let parser = new xml2js.Parser;
              parser.parseString(rawToken, (err, result) =>{
                  if(err){
                    console.log(err);
                    reject(err)
                  }else{
                    dataReturn = result.Assertion.AttributeStatement
                    console.log('AttributeStatement :',result.Assertion.AttributeStatement);
                    resolve(result)
                  }
                  
              });
           })
          })

          await aa;
      
          

          return response.json({
            status: "success",
            data: dataReturn
          });
        } catch (err) {
          console.log(err)
          return response
            .status(err.status)
            .send(err)
        }
    }

}

module.exports = ADFSController
