'use strict'
const fs = use("fs");
const Helpers = use("Helpers");
const { promisify } = use('util');
const redis = use('redis');
//for XML parsing
var xml2js = use('xml2js');
var https = use('https');
//to process WS-Trust requests
var trustClient = use('wstrust-client');

const redisClient = redis.createClient();
const getAsync = promisify(redisClient.get).bind(redisClient);
class OrgController {
    async create ({ request, response }) {
        try {
          console.log('start')
          let rawdata = fs.readFileSync(Helpers.tmpPath("AvailablePositions.json"));

          let orgAll = JSON.parse(rawdata);

          let childs = []
          orgAll.AvailablePosition.childs.map( item => childs.push(item.objectType + ":" + item.objectId))
    
          redisClient.set( orgAll.AvailablePosition.objectType+":"+orgAll.AvailablePosition.objectId, JSON.stringify(
            {
            parent: null,
            objectType:  orgAll.AvailablePosition.objectType,
            objectId: orgAll.AvailablePosition.objectId,
            shortText: orgAll.AvailablePosition.shortText,
            longText: orgAll.AvailablePosition.longText,
            beginDate: orgAll.AvailablePosition.beginDate,
            endDate: orgAll.AvailablePosition.endDate,
            dflag: orgAll.AvailablePosition.dflag,
            taskId: orgAll.AvailablePosition.taskId,
            taskDesc: orgAll.AvailablePosition.taskDesc,
            positionGroupId: orgAll.AvailablePosition.positionGroupId,
            positionGroupDesc: orgAll.AvailablePosition.positionGroupDesc,
            childs:childs
            }
          ));
          this.setNestedChildren(orgAll.AvailablePosition.childs,orgAll.AvailablePosition.objectType + ":" + orgAll.AvailablePosition.objectId)
          return response.json({
            status: "success",
            data: true
          });
        } catch (err) {
          console.log(err)
          return response
            .status(err.status)
            .send(err)
        }
    }

    async setNestedChildren(obj, parent) {
        obj.map(current => {
            let childs = []
            current.childs.map( item => childs.push(item.objectType + ":" + item.objectId))

            redisClient.set( current.objectType+":"+current.objectId, JSON.stringify(
              {
                parent: parent,
                objectType:  current.objectType,
                objectId: current.objectId,
                shortText: current.shortText,
                longText: current.longText,
                beginDate: current.beginDate,
                endDate: current.endDate,
                dflag: current.dflag,
                taskId: current.taskId,
                taskDesc: current.taskDesc,
                positionGroupId: current.positionGroupId,
                positionGroupDesc: current.positionGroupDesc,
                childs:childs
              }
            ));
            this.setNestedChildren(current.childs, current.objectType+":"+current.objectId) 
        })
    }

    async getObjHierarchy ({ request,params, response }) {
        try {
          console.log('start')
          const data = await getAsync(params.key);
          let dataObj = JSON.parse(data);
          dataObj.parentsObj = []
          await this.getNestedChildren(dataObj)
          await this.getNestedParent(dataObj,dataObj)
          return response.json({
            status: "success",
            data: dataObj
          });
        } catch (err) {
          console.log(err)
          return response
            .status(err.status)
            .send(err)
        }
    }

     async getNestedChildren(obj) {

        try {
        obj.childsObj = []
        await Promise.all(obj.childs.map(async current => {
            const data = await getAsync(current);
            let dataObj = JSON.parse(data);
            obj.childsObj.push(dataObj)
            if(dataObj.childs.length > 0){
                await this.getNestedChildren(dataObj)
            }
            
        }))
       
        return obj

        } catch (err) {
            console.log(err)
        }

    }

    async getNestedParent(obj,recursionObj) {

        try {
        const data = await getAsync(recursionObj.parent);
        let dataObj = JSON.parse(data);
        
        if(dataObj.childs.length > 0)
        {
           let child0 = dataObj.childs[0]
            if(child0.startsWith('S')){
                const posData = await getAsync(child0);
                let posObj = JSON.parse(posData);
                dataObj.childsObj = []
                dataObj.childsObj.push(posObj)
                if(posObj.childs.length > 0){
                    const empData = await getAsync(posObj.childs[0]);
                    let empObj = JSON.parse(empData);
                    dataObj.childsObj[0].childsObj = []
                    dataObj.childsObj[0].childsObj.push(empObj)
                }
           }
        }
        obj.parentsObj.push(dataObj)
        await this.getNestedParent(obj,dataObj)
        return obj

        } catch (err) {
            console.log(err)
        }

    }

    async getOrgChartTest ({ request,params, response }) {
        try {
          console.log('start')
          const data = await getAsync(params.key);
          let dataObj = JSON.parse(data);
          dataObj.id = dataObj.objectId
          dataObj.title = dataObj.longText
          dataObj.name =  dataObj.objectType + ":" + (dataObj.objectType == "S" ? "("+dataObj.taskId+")" : + dataObj.objectId )
        
          await this.getNestedChildrenOrgChartTest(dataObj)
          return response.json({
            status: "success",
            data: dataObj
          });
        } catch (err) {
          console.log(err)
          return response
            .status(err.status)
            .send(err)
        }
    }

     async getNestedChildrenOrgChartTest(obj) {

        try {
        obj.children = []
        await Promise.all(obj.childs.map(async current => {
            const data = await getAsync(current);
            let dataObj = JSON.parse(data);
            dataObj.id = dataObj.objectId
            dataObj.title = dataObj.longText
            dataObj.name = dataObj.objectType + ":"  + (dataObj.objectType == "S" ? "("+dataObj.taskId+")" : + dataObj.objectId)
            obj.children.push(dataObj)
            if(dataObj.childs.length > 0){
                await this.getNestedChildrenOrgChartTest(dataObj)
            }
            
        }))
       
        return obj

        } catch (err) {
            console.log(err)
        }

    }


    async getADFSTest ({ request,params, response }) {
        try {
          console.log('start')
          
          let userName = req.body.user;
          let userPassword = req.body.password;
       
          //call endpoint, and pass in values
          trustClient.requestSecurityToken({
              scope: 'https://testadfs.gsb.or.th/adfs/services/npla',
              username: userName,
              password: userPassword,
              endpoint: 'https://testadfs.gsb.or.th/adfs/services/trust/13/usernamemixed'
          }, function (rstr) {
       
              // Access the token
              let rawToken = rstr.token;
              console.log('raw: ' + rawToken);
       
              //convert to json
              let parser = new xml2js.Parser;
              parser.parseString(rawToken, function(err, result){
                  //grab "user" object
                  console.log('result : ', result);
                  let user = result.Assertion.AttributeStatement[0].Attribute[0].AttributeValue[0];
                  //get all "roles"
                  let roles = result.Assertion.AttributeStatement[0].Attribute[1].AttributeValue;
                  console.log(user);
                  console.log(roles);
       
              });
          })

          return response.json({
            status: "success",
            data: null
          });
        } catch (err) {
          console.log(err)
          return response
            .status(err.status)
            .send(err)
        }
    }

}

module.exports = OrgController
